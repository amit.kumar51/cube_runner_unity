using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class GameController : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject tabToStart;
    public GameObject scoreText;
    // Start is called before the first frame update
    private void Start()
    {
        gameOverPanel.SetActive(false);
        tabToStart.SetActive(true);
        scoreText.SetActive(false);
        PauseGame();
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKey(KeyCode.Space))
        {
            StartGame();
        }
    }

    public void GameOver()
    {
        scoreText.SetActive(false);
        gameOverPanel.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Game");
    }

    public void Quit()
    {
        scoreText.SetActive(false);
        Application.Quit();
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void StartGame()
    {
        tabToStart.SetActive(false);
        scoreText.SetActive(true);
        Time.timeScale = 1f;
    }
}


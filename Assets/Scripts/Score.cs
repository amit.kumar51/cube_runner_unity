using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Score : MonoBehaviour
{
    public Text scoreText;
    public Text finalScoreText;
    int myScore = 0;

    // Update is called once per frame
    void Update()
    {
        scoreText.text = myScore.ToString();
        finalScoreText.text = myScore.ToString();
    }

    public void addScore(int score)
    {
        myScore = myScore + score;
    }
}

